int DMI = 3;
int RELEASE = 2;

char ReadChar;
//int flag = 0;

void func_flag() {
  Serial.write('a'); //조도 센서 추가
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  while (!Serial)
  {
    ;
  }
  
  digitalWrite(RELEASE, LOW);
  pinMode(RELEASE, OUTPUT);
  
  pinMode(DMI, INPUT); //INPUT or 
  attachInterrupt(digitalPinToInterrupt(DMI), func_flag, RISING);
}

void loop() {
  delayMicroseconds(1);
}


void serialEvent()
{
  while (Serial.available() > 0)
  {
    ReadChar = (char)Serial.read();
    if (ReadChar == 's')  //  || (ReadChar == 'S')
    {
      digitalWrite(RELEASE, HIGH);
      delay(20);
//      Serial.println("shoot!");
      digitalWrite(RELEASE, LOW);
    }else{
      Serial.println("unvalid key received");
    }
  }
}
