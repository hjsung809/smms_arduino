#define WHEEL_RADIUS              0.275  // 7.5 inch radius for morning
#define PULSE_PER_ROTATION        1000
#define TARGET_DISTANCE           10.0
#define TARGET_PULSE_NUMBER       10

#define DIVISOR 5

int EncoderPulseCount;
int pulse_number = 100;
int ENCODER_PULSE = 3;
int DMI_SIGNAL = 4;


void OccurEncoderPulse()
{
    EncoderPulseCount++;
}


void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  pinMode(DMI_SIGNAL, OUTPUT);
  pinMode(ENCODER_PULSE, INPUT);
  attachInterrupt(digitalPinToInterrupt(ENCODER_PULSE), OccurEncoderPulse, RISING);

  double diameter = 2 * PI * WHEEL_RADIUS;
  double distancePerRatation = TARGET_DISTANCE / diameter;
  pulse_number = distancePerRatation * PULSE_PER_ROTATION / TARGET_PULSE_NUMBER;
  EncoderPulseCount = 0;

  pulse_number = pulse_number/DIVISOR;
//  pulse_number = 254;
  Serial.println(pulse_number);
}

void loop() {
  
    if(EncoderPulseCount >= pulse_number)
    {
        digitalWrite(DMI_SIGNAL, HIGH);
        delay(1);
        digitalWrite(DMI_SIGNAL, LOW);
        
        EncoderPulseCount = EncoderPulseCount - pulse_number;
        Serial.println(EncoderPulseCount);
    }
    delayMicroseconds(1);
}
